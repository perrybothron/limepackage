/*
 *  blowpops.c
 *  LIME, The versatile 3D line modeling environment
 *
 *  Created by Christian Brinch on 14/11/07.
 *  Copyright 2006-2017, Christian Brinch,
 *  <brinch@nbi.dk>
 *  Niels Bohr institutet
 *  University of Copenhagen
 *	All rights reserved.
 *
 */
#ifdef QHULL_INC_QHULL
#include <qhull/qhull_a.h>
#else
#include <libqhull/qhull_a.h>
#endif
#include "lime.h"

void
popsout(inputPars *par, struct grid *g, molData *m){
  FILE *fp;
  int j,k,l;
  double dens;
  /* int i,mi,c,q=0,best; */
  /* double vel[3],ra[100],rb[100],za[100],zb[100],min; */

  if((fp=fopen(par->outputfile, "w"))==NULL){
    if(!silent) bail_out("Error writing output populations file!");
    exit(1);
  }
  fprintf(fp,"# Column definition: x, y, z, H2 density, kinetic gas temperature, molecular abundance, convergence flag, pops_0...pops_n\n");
  for(j=0;j<par->ncell-par->sinkPoints;j++){
    dens=0.;
    for(l=0;l<par->collPart;l++) dens+=g[j].dens[l];
    fprintf(fp,"%e %e %e %e %e %e %d ", g[j].x[0], g[j].x[1], g[j].x[2], dens, g[j].t[0], g[j].nmol[0]/dens, g[j].conv);
    for(k=0;k<m[0].nlev;k++) fprintf(fp,"%e ",g[j].mol[0].pops[k]);
    fprintf(fp,"\n");
  }
  fclose(fp);
}


void
binpopsout(inputPars *par, struct grid *g, molData *m){
  FILE *fp;
  int i,j;

  if((fp=fopen(par->binoutputfile, "wb"))==NULL){
    if(!silent) bail_out("Error writing binary output populations file!");
    exit(1);
  }

  fwrite(&par->radius,   sizeof(double), 1, fp);
  fwrite(&par->ncell,    sizeof(int), 1, fp);
  fwrite(&par->nSpecies, sizeof(int), 1, fp);

  for(i=0;i<par->nSpecies;i++){
    fwrite(&m[i].nlev,  sizeof(int),               1,fp);
    fwrite(&m[i].nline, sizeof(int),               1,fp);
    fwrite(&m[i].npart, sizeof(int),               1,fp);
    fwrite(m[i].ntrans, sizeof(int)*m[i].npart,    1,fp);
    fwrite(m[i].lal,    sizeof(int)*m[i].nline,    1,fp);
    fwrite(m[i].lau,    sizeof(int)*m[i].nline,    1,fp);
    fwrite(m[i].aeinst, sizeof(double)*m[i].nline, 1,fp);
    fwrite(m[i].freq,   sizeof(double)*m[i].nline, 1,fp);
    fwrite(m[i].beinstl,sizeof(double)*m[i].nline, 1,fp);
    fwrite(m[i].beinstu,sizeof(double)*m[i].nline, 1,fp);
    fwrite(m[i].local_cmb, sizeof(double)*m[i].nline,1,fp);
    fwrite(&m[i].norm,  sizeof(double),1,fp);
    fwrite(&m[i].norminv,sizeof(double),1,fp);
  }

  for(i=0;i<par->ncell;i++){
    fwrite(&g[i].id,   sizeof(int),      1, fp);
    fwrite(&g[i].x,  3*sizeof(double),   1, fp);
    fwrite(&g[i].vel,3*sizeof(double),   1, fp);
    fwrite(&g[i].sink, sizeof(int),      1, fp);
    fwrite(g[i].nmol,  sizeof(double)*par->nSpecies,1, fp);
    fwrite(&g[i].dopb, sizeof g[i].dopb, 1, fp);
    for(j=0;j<par->nSpecies;j++){
      fwrite(g[i].mol[j].pops,  sizeof(double)*m[j].nlev, 1, fp);
      fwrite(g[i].mol[j].knu,   sizeof(double)*m[j].nline,1, fp);
      fwrite(g[i].mol[j].dust,  sizeof(double)*m[j].nline,1, fp);
      fwrite(&g[i].mol[j].dopb, sizeof(double),           1, fp);
      fwrite(&g[i].mol[j].binv, sizeof(double),           1, fp);
    }
    fwrite(&g[i].dens[0], sizeof(double), 1, fp);
    fwrite(&g[i].t[0],    sizeof(double), 1, fp);
    fwrite(&g[i].abun[0], sizeof(double), 1, fp);
 }


  fclose(fp);

}

void
sourceFuncOut(inputPars *par, struct grid *g, molData *m){
  FILE *fp;
  int j,k,l,here;
  double dens;
  double snu_line,snu_cont,dtau,j_line,alpha_line,j_cont,alpha_cont,vfac;
  double x[3], inidir[3],vel[3];
  int *counta, *countb, nlinetot;

  if((fp=fopen(par->sourceoutputfile, "w"))==NULL){
    if(!silent) bail_out("Error writing output source function file!");
    exit(1);
  }

  lineCount(par->nSpecies, m, &counta, &countb, &nlinetot);

  vfac=1.0;

  fprintf(fp,"# No. of lines = %d\n", nlinetot);
  fprintf(fp,"# Column definition: x, y, z, H2 density, kinetic gas temperature, molecular abundance, convergence flag, k=0..nline: snu_line(k), snu_cont(k)\n");
  for(j=0;j<par->ncell-par->sinkPoints;j++){
    here=g[j].id;

    dens=0.;
    for(l=0;l<par->collPart;l++) dens+=g[j].dens[l];

    fprintf(fp,"%e %e %e %e %e %e %d ", g[j].x[0], g[j].x[1], g[j].x[2], dens, g[j].t[0], g[j].nmol[0]/dens, g[j].conv);
    for(k=0;k<nlinetot;k++){
      j_line = 0.; alpha_line = 0.;
      j_cont = 0.; alpha_cont = 0.;
      sourceFunc_line(&j_line,&alpha_line,m,vfac,g,here,counta[k],countb[k]);
      sourceFunc_cont(&j_cont,&alpha_cont,g,here,counta[k],countb[k]);
      snu_line = (j_line / alpha_line);
      snu_cont = (j_cont / alpha_cont);
      fprintf(fp,"%e %e ",snu_line,snu_cont);
    }
    fprintf(fp,"\n");
  }
  fclose(fp);
}

void
write_VTK_LvlPops_SourceFunc(inputPars *par, struct grid *g, molData *m){
  FILE *fp;
  double length;
  int i,j,k,l=0;
  char flags[255];
  boolT ismalloc = False;
  facetT *facet;
  vertexT *vertex,**vertexp;
  coordT *pt_array;
  int curlong, totlong;
  int *counta, *countb, nlinetot, here;
  double snu_line,j_line,alpha_line,snu_cont,j_cont,alpha_cont,vfac;

  lineCount(par->nSpecies, m, &counta, &countb, &nlinetot);
  vfac=1.0;

  pt_array=malloc(sizeof(coordT)*DIM*par->ncell);

  for(i=0;i<par->ncell;i++) {
    for(j=0;j<DIM;j++) {
      pt_array[i*DIM+j]=g[i].x[j];
    }
  }

  if((fp=fopen(par->popssourceVTKfile, "w"))==NULL){
    if(!silent) bail_out("write_VTK_source_func: Error writing file!");
    exit(1);
  }
  fprintf(fp,"# vtk DataFile Version 3.0\n");
  fprintf(fp,"Lime grid and source function, nlinetot=%d\n", nlinetot);
  fprintf(fp,"ASCII\n");
  fprintf(fp,"DATASET UNSTRUCTURED_GRID\n");
  fprintf(fp,"POINTS %d float\n",par->ncell);
  for(i=0; i<par->ncell; i++) {
    fprintf(fp,"%e %e %e\n", g[i].x[0], g[i].x[1], g[i].x[2]);
  }
  fprintf(fp, "\n");

  sprintf(flags,"qhull d Qbb T0");

  if (!qh_new_qhull(DIM, par->ncell, pt_array, ismalloc, flags, NULL, NULL)) {
    FORALLfacets {
      if (!facet->upperdelaunay) l++;
    }
    fprintf(fp,"CELLS %d %d\n",l, 5*l);
    FORALLfacets {
      if (!facet->upperdelaunay) {
        fprintf(fp,"4 ");
        FOREACHvertex_ (facet->vertices) {
          fprintf(fp, "%d ", qh_pointid(vertex->point));
        }
        fprintf(fp, "\n");
      }
    }
  }
  qh_freeqhull(!qh_ALL);
  qh_memfreeshort (&curlong, &totlong);
  fprintf(fp,"\nCELL_TYPES %d\n",l);
  for(i=0;i<l;i++){
    fprintf(fp, "10\n");
  }
  fprintf(fp,"POINT_DATA %d\n",par->ncell);
  fprintf(fp,"SCALARS H2_density float 1\n");
  fprintf(fp,"LOOKUP_TABLE default\n");
  for(i=0;i<par->ncell;i++){
    fprintf(fp, "%e\n", g[i].dens[0]);
  }
  fprintf(fp,"SCALARS Mol_abundance float 1\n");
  fprintf(fp,"LOOKUP_TABLE default\n");
  for(i=0;i<par->ncell;i++){
    fprintf(fp, "%e\n", g[i].abun[0]);
  }
  for(k=0;k<nlinetot;k++){
    fprintf(fp,"SCALARS sourcefunc_line%d float 2\n", k);
    fprintf(fp,"LOOKUP_TABLE default\n");
    for(i=0;i<par->ncell;i++){
      if (g[i].sink) {
        snu_line = 0.0;
        snu_cont = 0.0;
      } else {
        here=g[i].id;
        j_line = 0.; alpha_line = 0.;
        sourceFunc_line(&j_line,&alpha_line,m,vfac,g,here,counta[k],countb[k]);
        sourceFunc_cont(&j_cont,&alpha_cont,g,here,counta[k],countb[k]);
        snu_line = (j_line / alpha_line);
        snu_cont = (j_cont / alpha_cont);
        if (isnan(snu_line)) snu_line=0.0;
        if (isnan(snu_cont)) snu_cont=0.0;
      }
      fprintf(fp, "%e %e\n", snu_line, snu_cont);
    }
  }
  for(k=0;k<m[0].nlev;k++){
    fprintf(fp,"SCALARS fractional_pop_level%d float 1\n", k);
    fprintf(fp,"LOOKUP_TABLE default\n");
    for(i=0;i<par->ncell;i++){
      if (g[i].sink) {
        fprintf(fp,"%e\n",0.0);
      } else {
        fprintf(fp,"%e\n",g[i].mol[0].pops[k]);
      }
    }
  }

  fclose(fp);
  free(pt_array);
}

void
dumpLvlPopsSourceFuncVTK(inputPars *par, struct grid *g, molData *m){
  if(par->popssourceVTKfile) write_VTK_LvlPops_SourceFunc(par, g, m);
}
